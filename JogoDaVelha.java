
public class JogoDaVelha {

    public static void main(String[] args) {
        
        // Cria uma instância do jogo
        Jogo jogoDaVelha = new Jogo();

        // Teste de termino
        boolean terminou = false;

        // Imprime a matriz vazia
        jogoDaVelha.imprimeMatriz();

        do {
            // Jogada do jogador 1
            jogoDaVelha.jogada(1);

            // Imprime a matriz
            jogoDaVelha.imprimeMatriz();

            // Testa se venceu
            if (jogoDaVelha.vitoria() == 1) {

                System.out.println ("Jogador 1 venceu!!!");

                terminou = true;
            }

            if (!terminou)

                terminou = jogoDaVelha.empate();
                
            if (!terminou) {
                // Jogada do jogador 2
                jogoDaVelha.jogada(2);

                // Imprime a matriz
                jogoDaVelha.imprimeMatriz();

                // Testa se venceu
                if (jogoDaVelha.vitoria() == 2) {

                    System.out.println ("Jogador 2 venceu!!!");

                    terminou = true;
                }

            }

            if (!terminou)

                terminou = jogoDaVelha.empate();

        } while (! terminou);

    }


}