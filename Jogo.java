import java.util.*;

public class Jogo {

    // Declaração da matriz para o tabuleiro
    private int tabuleiro [][];

    public Jogo () {

        tabuleiro = new int [3][3];

        // Iniciação da matriz com 0 em cada posição
        for (int i = 0; i < 3; ++ i) 
            for (int j = 0; j < 3; ++ j) 
                tabuleiro[i][j] = 0;

    }

    private boolean testaMarcado (int linha, int coluna) {

        return tabuleiro [linha][coluna] == 0 ? false : true;
            
    }

    public void imprimeMatriz () {

        // Imprime os valores da matriz
        //+---+---+---+
        //|   |   |   |
        //+---+---+---+
        //|   |   |   |
        //+---+---+---+
        //|   |   |   |
        //+---+---+---+

        System.out.println ("   0   1   2");
        System.out.println (" +---+---+---+");

        for (int linha = 0; linha < 3; ++ linha) {

            
            System.out.print (linha + "|");

            for (int coluna = 0; coluna < 3; ++ coluna) {

                String valorImpresso = " ";
                switch (tabuleiro [linha][coluna]) {

                    case 0 : 
                        valorImpresso = " ";
                        break;

                    case 1 : 
                        valorImpresso = "X";
                        break;

                    case 2 : 
                        valorImpresso = "O";
                        break;
                }


                System.out.print (" " + valorImpresso + " |");
            }

            System.out.println();
            System.out.println (" +---+---+---+");

        }
        
        System.out.println();

    }

    public void jogada (int jogador) {

        // Valor da linha
        int linha;

        // Valor da coluna
        int coluna;

        // Leitura de valores
        Scanner leitura = new Scanner (System.in);;

        System.out.println ("Jogador " + jogador + ": ");

        do {

            System.out.print ("Linha: ");
            linha = leitura.nextInt();

            System.out.print ("Coluna: ");
            coluna = leitura.nextInt();

            System.out.println ();

            if (testaMarcado(linha, coluna))
                System.out.println ("Local já marcado!!!");

        } while (testaMarcado (linha, coluna));

        tabuleiro [linha][coluna] = jogador;
    }

    public int vitoria () {
        // Retorna 0 se ninguem venceu ainda
        //          1 se o jogador 1 venceu
        //          2 se o jogador 2 venceu

        // Testar as linhas
        int conta;

        for (int jogador = 1; jogador <= 2; ++ jogador) {

            for (int linha = 0; linha < 3; ++ linha) {

                conta = 0;
                for (int coluna = 0; coluna < 3; coluna ++)
                {

                    if (tabuleiro [linha][coluna] == jogador) conta ++;

                }

                if (conta == 3) return jogador;
            }
        }

        // Testar as colunas
        for (int jogador = 1; jogador <= 2; ++ jogador) {

            for (int coluna = 0; coluna < 3; ++ coluna) {

                conta = 0;
                for (int linha = 0; linha < 3; linha ++)
                {

                    if (tabuleiro [linha][coluna] == jogador) conta ++;

                }

                if (conta == 3) return jogador;
            }
        }

        // Testar as diagonais
        for (int jogador = 1; jogador <= 2; ++ jogador) {
            
            if (tabuleiro [0][0] == jogador &&
                tabuleiro [1][1] == jogador &&
                tabuleiro [2][2] == jogador  
                ) 
                
                return jogador;

            if (tabuleiro [0][2] == jogador &&
                tabuleiro [1][1] == jogador &&
                tabuleiro [2][0] == jogador  
                ) 
                
                return jogador;
            
        }

        return 0;

    }

    public boolean empate () {

        boolean foiEmpate;

        int contaZeros = 0;

        for (int i = 0; i < 3; ++ i) 
            for (int j = 0; j < 3; ++ j) 
                if (tabuleiro[i][j] == 0) 
                    ++contaZeros;
        
        if (contaZeros == 0) {
         
            System.out.println ("Empate!!!");

            foiEmpate = true;
        }
        else
            foiEmpate = false;

        
        return foiEmpate;
    }


}